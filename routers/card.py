from fastapi import APIRouter, Depends

from models import DebitCard, Bank
from routers.common import get_bank

router = APIRouter(
    tags=["card"],
    prefix="/banks/{bank_id}/cards"
)


@router.get("")
async def get_cards(bank: Bank = Depends(get_bank)) -> list[DebitCard]:
    cards = bank.cards.get_all()
    return cards
