from fastapi import APIRouter, Depends

from models import Account, Bank
from routers.common import get_bank

router = APIRouter(
    tags=["account"],
    prefix="/banks/{bank_id}/accounts"
)


@router.get("")
async def get_accounts(bank: Bank = Depends(get_bank)) -> list[Account]:
    bank_accounts = bank.accounts.get_all()
    return bank_accounts
