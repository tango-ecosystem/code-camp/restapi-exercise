from fastapi import APIRouter, Depends

from models import Atm, Bank
from routers.common import get_bank

router = APIRouter(
    tags=["atm"],
    prefix="/banks/{bank_id}/atms"
)


@router.get("")
async def get_atms(bank: Bank = Depends(get_bank)) -> list[Atm]:
    bank_atms = bank.atms.get_all()
    return bank_atms
