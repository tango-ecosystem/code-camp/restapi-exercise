from typing import Optional

from fastapi import APIRouter, Depends, HTTPException, Response

from common.constants import Nationalities
from common.utils import SequencerGenerator
from models import Bank
from manager.storage import get_storage, Storage
from routers.common import get_bank

router = APIRouter(
    tags=["bank"],
    prefix="/banks"
)


@router.get("")
async def get_banks(storage: Storage = Depends(get_storage)) -> list[Bank]:
    return storage.banks.get_all()


@router.post("")
async def create_bank(name: str, address: str, phone: str, email: str, country: Nationalities,
                      response: Response, storage: Storage = Depends(get_storage)) -> Bank:
    new_id = SequencerGenerator().generate_sequence(10)
    new_bank = Bank(id=new_id, name=name, address=address, phone=phone, email=email, country=country)
    bank = storage.banks.add(new_bank)
    if not bank:
        raise HTTPException(status_code=500, detail="Bank could not be created")
    response.status_code = 201
    return new_bank


@router.get("/{bank_id}")
async def get_bank(bank: Bank = Depends(get_bank)) -> Bank:
    return bank


@router.put("/{bank_id}")
async def update_bank(name: str = None, address: str = None, phone: str = None,
                      email: str = None, bank: Bank = Depends(get_bank)) -> Bank:
    bank.name = name if name else bank.name
    bank.address = address if address else bank.address
    bank.phone = phone if phone else bank.phone
    bank.email = email if email else bank.email
    return bank


@router.delete("/{bank_id}")
async def delete_bank(bank: Bank = Depends(get_bank), storage: Storage = Depends(get_storage)) -> dict:
    removed = storage.banks.remove(bank)
    if not removed:
        raise HTTPException(status_code=500, detail="Bank could not be deleted")
    return {"message": "Bank deleted successfully"}
