from fastapi import APIRouter, Depends, HTTPException

from models import Customer, Bank
from routers.common import get_bank

router = APIRouter(
    tags=["customer"],
    prefix="/banks/{bank_id}/customers"
)


@router.get("")
async def get_customers(bank: Bank = Depends(get_bank)) -> list[Customer]:
    return bank.customers.get_all()


@router.post("")
async def create_customer(first_name: str, last_name: str, email: str, bank: Bank = Depends(get_bank)) -> Customer:
    new_customer = Customer(first_name=first_name, last_name=last_name, email=email)
    created = bank.customers.add(new_customer)
    if not created:
        raise HTTPException(status_code=400, detail="Customer already exists")
    return new_customer


@router.get("/{customer_email}")
async def get_customer(customer_email: str, bank: Bank = Depends(get_bank)) -> Customer:
    customer = bank.customers.get(customer_email)
    if customer is None:
        raise HTTPException(status_code=404, detail="Customer not found")
    return customer


@router.put("/{customer_email}")
async def update_customer(customer_email: str, first_name: str, last_name: str,
                          bank: Bank = Depends(get_bank)) -> Customer:
    customer = bank.customers.get(customer_email)
    if customer is None:
        raise HTTPException(status_code=404, detail="Customer not found")
    customer.first_name = first_name if first_name else customer.first_name
    customer.last_name = last_name if last_name else customer.last_name
    return customer


@router.delete("/{customer_id}")
async def delete_customer(customer_id: str, bank: Bank = Depends(get_bank)) -> dict:
    customer = bank.customers.get(customer_id)
    if customer is None:
        raise HTTPException(status_code=404, detail="Customer not found")
    bank.customers.remove(customer)
    return {"message": "Customer deleted successfully"}
