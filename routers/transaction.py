from fastapi import APIRouter, Depends

from models import Transaction, Bank
from routers.common import get_bank

router = APIRouter(
    tags=["transaction"],
    prefix="/banks/{bank_id}/transactions"
)


@router.get("")
async def get_transactions(bank: Bank = Depends(get_bank)) -> list[Transaction]:
    bank_transactions = bank.transactions.get_all()
    return bank_transactions
