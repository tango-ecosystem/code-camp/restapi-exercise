from fastapi import Depends, HTTPException

from manager.storage import Storage, get_storage
from models import Bank


async def get_bank(bank_id: str, storage: Storage = Depends(get_storage)) -> Bank:
    bank = storage.banks.get(bank_id)
    if bank is None:
        raise HTTPException(status_code=404, detail="Bank not found")
    return bank
