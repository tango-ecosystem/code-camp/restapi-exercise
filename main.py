from fastapi import FastAPI
from routers.bank import router as bank_router
from routers.atm import router as atm_router
from routers.transaction import router as transaction_router
from routers.account import router as account_router
from routers.card import router as card_router
from routers.customer import router as customer_router

app = FastAPI()

app.include_router(bank_router)
app.include_router(atm_router)
app.include_router(transaction_router)
app.include_router(account_router)
app.include_router(card_router)
app.include_router(customer_router)
