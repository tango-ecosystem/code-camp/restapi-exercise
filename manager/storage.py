"""
    This module contains the Storage.
    Storage acts as an in-memory database to store the instances of the models.
"""
from manager.model_list import ModelList
from models import Bank


class Storage:

    def __init__(self):
        self.banks = ModelList[Bank]()


storage = Storage()


async def get_storage():
    try:
        yield storage
    finally:
        pass
