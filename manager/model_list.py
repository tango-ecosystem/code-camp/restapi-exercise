from typing import TypeVar, Generic, Optional

from pydantic import BaseModel

T = TypeVar('T', bound=BaseModel)


class ModelList(Generic[T]):
    """
    A generic class to store model instances.
    Contains methods to add, get, get all, remove, and iterate over the instances.
    The generic class needs to provide an 'id' attribute or property.
    """

    def __init__(self):
        self.data = []

    def add(self, obj: T) -> bool:
        """
        Add the model instance to the list.

        :param obj: The model instance to add.
        :return: True if the instance is added, False otherwise.
        """
        if not hasattr(obj, "id"):
            raise AttributeError(f"{T.__name__} has no attribute 'id'")
        if self.get(obj.id):
            return False
        self.data.append(obj)
        return True

    def get(self, model_id: str) -> Optional[T]:
        """
        Get the model instance by its id.

        :param model_id: The id of the model instance.
        :return: The model instance if found, None otherwise.
        """
        for obj in self.data:
            if hasattr(obj, "id"):
                if obj.id == model_id:
                    return obj
            else:
                raise AttributeError(f"{T.__name__} has no attribute 'id'")
        return None

    def get_all(self) -> list[T]:
        """
        Get all instances in the list.

        :return: A list of model instances.
        """
        return self.data

    def remove(self, obj: T) -> bool:
        """
        Remove the model instance from the list.

        :param obj: The model instance to remove.
        :return: True if the instance is removed, False otherwise.
        """
        if not hasattr(obj, "id"):
            raise AttributeError(f"{T.__name__} has no attribute 'id'")
        element = self.get(obj.id)
        if element:
            self.data.remove(obj)
            return True
        return False

    def __len__(self):
        return len(self.data)

    def __getitem__(self, model_id: str) -> T:
        return self.get(model_id)

    def __iter__(self):
        return iter(self.data)

    def __next__(self):
        return next(self.data)

    def __contains__(self, item: T):
        return item in self.data

    def __repr__(self):
        return f"{self.__class__.__name__}({self.data})"
