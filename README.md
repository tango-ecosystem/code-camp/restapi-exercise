# RestAPI Exercise


## Project details

This project contains a mock server for the restapi exercise.
`FastAPI` framework was used as foundation of the project, and `Pydantic` models are implemented to map the models of the data.


# Installation

To install the project, first clone the repository and then install the requirements.
Open the project directory and execute the following command:

```
pip install -r requirements.txt
```


## Running the project

To start the project, enter the src folder and run the following code:

```
uvicorn main:app --reload
```

With the reload parameter set, the server restarts automatically every time a file is changed.
The default port used is `8000`. In order to set a different port use the `--port` parameter:
```
uvicorn main:app --reload --port 8086
```

If running the project in a local environment, you can access the server at `127.0.0.1:8000`.
In the home path, you only get '{"detail":"Not Found"}' as nothing is defined for that path.
To view details about the implemented endpoints, you can access the documentation


## Documentation

The FastAPI framework automatically generates a documentation for the implemented endpoints.
Two openapi documentations are generated:
* A Swagger UI documentation at `/docs`
* A Redoc documentation at `/redoc`


## FastAPI framework

FastAPI is a modern, fast (high-performance), web framework for building APIs with Python.
You can define different routers to split the endpoints in different files.
To define an endpoint in a router, you can use the `@router.get` decorator. 
This creates an endpoint that listens to the GET requests in the specified path.
Besides the `@router.get` decorator, there are also `@router.post`, `@router.put`, `@router.delete` decorators.
Here is an example of a router:

```
from fastapi import APIRouter
    
router = APIRouter()

@router.get("/items/{item_id}")
async def read_item(item_id: int, q: str = None) -> dict:
    return {"item_id": item_id, "q": q}
```


## Project storage

The project uses a simple in-memory storage to store the data, which is defined in the file `storage.py`.
In the storage, a list of banks is stored.
To import the storage in a router, you can:
```
from manager.storage import Storage
```
Additionally, every bank define the list of accounts, customers, atms, cards and transactions it has.
Every one of the list can be read or edited by using the methods: `get`, `get_all`, `add`, `remove`.
An example of how to use the storage to operate on banks is shown below:

```
from manager.storage import Storage

storage = Storage()
banks = banks.get_all() # Get all the banks
bank = banks.get(1) # Get the bank with id 1
banks.add(bank) # Add a bank
banks.remove(bank) # Remove the bank
```

In order to retrieve the storage from an endpoint, you can define it in the endpoint parameters:

```
@router.get("/banks/{bank_id}")
async def read_bank(bank_id: int, storage: Storage = Depends(get_storage)) -> Bank:
    bank = storage.banks.get(bank_id)
    return bank
```

But you can also use this method to retrieve directly the bank we are operating on:

```
from routers.common import get_bank

@router.get("/banks/{bank_id}")
async def read_bank(bank: Bank = Depends(get_bank)) -> Bank:
    return bank
```

If you need to retrieve the list of customers of a bank, you can use the following code:

```
@router.get("/banks/{bank_id}/customers")
async def read_customers(bank: Bank = Depends(get_bank) -> List[Customer]:
    # You can also call bank.atms, bank.cards, bank.accounts, bank.transactions in the same way
    return bank.customers.get_all()
```

And to get a specific customer, you can use the following code:

```
@router.get("/banks/{bank_id}/customers/{customer_id}")
async def read_customer(customer_id: int, bank: Bank = Depends(get_bank)) -> Customer:
    customer = bank.customers.get(customer_id)
    return customer
```

## Project models

The project uses Pydantic models to map the data models.
The models are defined inside the `models` folder.
To import the models in a router, you can:

```
from models import Bank, Account, Customer, Atm, Card, Transaction
```

You can view what each model contain by opening the file defined in the `models` folder.

To instantiate a model (E.g. A customer), you can use the following code:

```
new_customer = Customer(first_name="John", last_name="Doe", email="john.doe@email.com")
```

Every model defines an `id` attribute or property, which is used to identify the model in the storage.
