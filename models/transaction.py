from pydantic import BaseModel

from datetime import datetime

from common.constants import TransactionType, TransactionStatus


class Transaction(BaseModel):
    id: int
    atm_sn: str
    card_number: str
    transaction_type: TransactionType
    transaction_status: TransactionStatus
    datetime: datetime
    amount: float

    def __str__(self):
        return (f"Transaction id: {self.id}, Type: {self.transaction_type}, Status: {self.transaction_status}, "
                f"Amount: {self.amount}, Date: {self.datetime}")
