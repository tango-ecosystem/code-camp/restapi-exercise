import logging

from pydantic import BaseModel

from common.constants import AccountStatus
from models.customer import Customer


class Account(BaseModel):
    account_number: str
    balance: float = 0
    customer: Customer
    account_status: AccountStatus

    @property
    def id(self) -> str:
        return self.account_number

    def deposit(self, amount: float) -> bool:
        if self.is_active is False:
            return False

        if amount <= 0:
            return False
        self.balance += amount
        return True

    def withdraw(self, amount: float) -> bool:
        if self.is_active is False:
            return False

        if amount <= 0:
            return False

        try:
            # The setter will raise an exception if the balance become negative
            self.balance -= amount
            return True
        except ValueError:
            logging.log(logging.ERROR, f"[Account] [{self.account_number}] - Withdrawal failed")
            return False
