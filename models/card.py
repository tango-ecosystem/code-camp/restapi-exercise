from datetime import datetime

from pydantic import BaseModel

from models import Customer, Account


class DebitCard(BaseModel):
    card_number: str
    customer: Customer
    expiration_date: datetime
    account: Account

    @property
    def id(self):
        return self.card_number

    def __str__(self):
        return f"Card number: {self.card_number}, Expiry Date: {self.expiry_date}, Account: {self.account}"
