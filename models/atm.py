from pydantic import BaseModel

from manager.model_list import ModelList
from models import Transaction


class Atm(BaseModel):
    serial_number: str
    name: str
    address: str
    bank_id: str

    @property
    def id(self):
        return self.serial_number

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__transactions = ModelList[Transaction]()

    @property
    def transactions(self) -> ModelList[Transaction]:
        return self.__transactions

    def __str__(self):
        return f"Atm name: {self.name}, Address: {self.address}, Serial Number: {self.serial_number}"
