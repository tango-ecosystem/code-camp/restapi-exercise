from typing import Optional

from pydantic import BaseModel

from common.constants import Nationalities
from manager.model_list import ModelList
from models import Atm, Customer, Account, DebitCard, Transaction


class Bank(BaseModel):
    id: str
    name: str
    address: str
    phone: Optional[str]
    email: Optional[str]
    country: Nationalities

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__atms = ModelList[Atm]()
        self.__customers = ModelList[Customer]()
        self.__accounts = ModelList[Account]()
        self.__cards = ModelList[DebitCard]()
        self.__transactions = ModelList[Transaction]()

    @property
    def atms(self) -> ModelList[Atm]:
        return self.__atms

    @property
    def customers(self) -> ModelList[Customer]:
        return self.__customers

    @property
    def accounts(self) -> ModelList[Account]:
        return self.__accounts

    @property
    def cards(self) -> ModelList[DebitCard]:
        return self.__cards

    @property
    def transactions(self) -> ModelList[Transaction]:
        return self.__transactions

    def __str__(self):
        return f"Bank name: {self.name}, Address: {self.address}, Country: {self.country}"
