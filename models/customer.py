from pydantic import BaseModel


class Customer(BaseModel):
    email: str
    first_name: str
    last_name: str

    @property
    def id(self) -> str:
        return self.email

    def full_name(self) -> str:
        return f"{self.first_name} {self.last_name}"

    def __str__(self) -> str:
        return f"{self.full_name()}, Email: {self.email}"
