from enum import Enum


class Nationalities(Enum):
    US = "United States"
    UK = "United Kingdom"
    FR = "France"
    DE = "Germany"
    IT = "Italy"
    ES = "Spain"
    JP = "Japan"
    CN = "China"
    IN = "India"


class TransactionType(Enum):
    WITHDRAWAL = "withdrawal"
    DEPOSIT = "deposit"


class TransactionStatus(Enum):
    SUCCESS = "success"
    FAILED = "failed"
    ABORTED = "aborted"


class AccountStatus(Enum):
    ACTIVE = "Active"
    SUSPENDED = "Suspended"
    CLOSED = "Closed"
