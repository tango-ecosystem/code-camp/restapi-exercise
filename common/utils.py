import uuid
import random
from typing import Optional
from datetime import date, timedelta
from common.constants import Nationalities


class DateGenerator:

    @staticmethod
    def generate_future_date(start_date: date) -> date:
        """
        Generate a date.
        The date is generated in the future with respect to the current date.
        In case the start_date is provided, the date is generated in the future with respect to the start_date.
        :param start_date: The start date
        :return: The generated date
        """
        return start_date + timedelta(weeks=random.randint(52, 200))


class SequencerGenerator:

    @staticmethod
    def generate_uuid() -> str:
        """
        Generate a UUID
        :return: The generated UUID
        """
        return str(uuid.uuid4())

    @staticmethod
    def generate_sequence(
            length: int,
            alphanumeric: bool = False,
    ) -> str:
        """
        Generate a sequence of random characters.
        :param length: The length of the sequence
        :param alphanumeric: Whether the sequence should be alphanumeric.
        Default is False so the sequence will be numeric.
        :return: The generated sequence
        """
        if alphanumeric:
            return ''.join(random.choices('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', k=length))
        return ''.join(random.choices('0123456789', k=length))

    @staticmethod
    def generate_serial_number(length: Optional[int] = 10) -> str:
        """
        Generate a serial number.
        Serial number is alphanumeric and has a default length of 10.
        :param length: The length of the serial number
        :return: The generated serial number
        """
        return SequencerGenerator.generate_sequence(length, alphanumeric=True)

    @staticmethod
    def generate_cvv() -> int:
        return random.randint(100, 999)

    @staticmethod
    def generate_card_number() -> int:
        return random.randint(10 ** 15, 10 ** 16 - 1)

    @staticmethod
    def generate_iban(
            country_code: Nationalities,
            check_code: Optional[str] = None,
            bank_code: Optional[str] = None,
            branch_code: Optional[str] = None,
            account_number: Optional[str] = None,
    ) -> str:
        """
        Generate an IBAN.
        Despite the fact that IBANs are country-specific, this method generates a generic IBAN.
        In case the parameters are not provided, the method generates random values
        creating an IBAN of 22 characters.
        :param country_code: The country code of the IBAN
        :param check_code: The check code of the IBAN
        :param bank_code: The bank code of the IBAN
        :param branch_code: The branch code of the IBAN
        :param account_number: The account number of the IBAN
        :return: The generated IBAN
        """
        # selecting random nationality from Nationalities enum
        check_code = random.choice([n.value for n in Nationalities]) if check_code is None else check_code
        bank_code = SequencerGenerator.generate_sequence(4, True) if bank_code is None else bank_code
        branch_code = SequencerGenerator.generate_sequence(6) if branch_code is None else branch_code
        account_number = SequencerGenerator.generate_sequence(10) if account_number is None else account_number
        return f"{country_code.value}{check_code}{bank_code}{branch_code}{account_number}"
